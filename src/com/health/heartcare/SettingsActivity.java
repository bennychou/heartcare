package com.health.heartcare;

import java.io.File;
import java.io.FileFilter;
import java.net.URLEncoder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.addon.IAddonBasicAttacher;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.widget.BaseExpandableListAdapter;
import org.holoeverywhere.widget.ExpandableListView;
import org.holoeverywhere.widget.ExpandableListView.OnChildClickListener;
import org.holoeverywhere.widget.TextView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class SettingsActivity extends SherlockFragmentActivity {
	public static String Settings = "Settings";
	public static String YourName = "YourName";
	public static String YourPhone = "YourPhone";
	public static String DoctorName = "DoctorName";
	public static String DoctorEmail = "DoctorEmail";
	public static String FamilyName = "FamilyName";
	public static String FamilyPhone = "FamilyPhone";
	
	File [] listFile;
	
	ExpandableListView expandableListView;
	ExpandableListAdapter adapter;
	SharedPreferences settingPrefs;
	
	String yourName;
	String yourPhone;
	String doctorName;
	String doctorEmail;
	String familyName;
	String familyPhone;
	
	private static final String[] Group = new String[] {"您的聯絡資訊", "醫師的聯絡資訊", "家人的聯絡資訊", "歷史記錄"};
	private static final String[][] Child = {
		{ "姓名：", "電話：" },
        { "姓名：", "電子郵件：" },
        { "姓名：", "電話：" },
        { "1", "2", "3", "4", "5" }
	};
	
	final String TAG = "SettingsActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setTheme(R.style.Holo_Theme_Light_DarkActionBar); // set the theme

		requestWindowFeature(com.actionbarsherlock.view.Window.FEATURE_INDETERMINATE_PROGRESS);

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
			requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.activity_settings);
		
		getSherlock().setProgressBarIndeterminateVisibility(false);
		
		/* set action bar */
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowHomeEnabled(true);
		actionBar.setLogo(getResources().getDrawable(R.drawable.ic_main_logo));
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.ic_main_actionbar));
		
		/* set the prefs */
		settingPrefs = getSharedPreferences(Settings, Context.MODE_PRIVATE);
		yourName = settingPrefs.getString(YourName, MainActivity.patientName);
		yourPhone = settingPrefs.getString(YourPhone, MainActivity.patientNumber);
		doctorName = settingPrefs.getString(DoctorName, MainActivity.doctorName);
		doctorEmail = settingPrefs.getString(DoctorEmail, MainActivity.doctorEmail);
		familyName = settingPrefs.getString(FamilyName, "");
		familyPhone = settingPrefs.getString(FamilyPhone, MainActivity.patientEmergency);
		
		/* get the historical file */
		String localPath = Environment.getExternalStorageDirectory().getPath() +File.separator+ MainActivity.saveFolder;
		File file = new File(localPath);
		FileFilter fileFileter = new FileFilter() {
			
			@Override
			public boolean accept(File pathname) {
				// TODO Auto-generated method stub
				if (pathname.isFile() && pathname.exists() && pathname.getName().toLowerCase().endsWith("csv"))
					return true;
				return false;
			}
		};
		listFile = file.listFiles(fileFileter);
		
		findViews();
		
		adapter = new ExpandableListAdapter();
		expandableListView.setAdapter(adapter);
		
		setListeners();
	}
	
	private void findViews() {
		expandableListView = (ExpandableListView) findViewById(R.id.list);
	}
	
	private void setListeners() {
		expandableListView.setOnChildClickListener(childClickListener);
	}
	
	public void refreshAdapter() {
		yourName = settingPrefs.getString(YourName, MainActivity.patientName);
		yourPhone = settingPrefs.getString(YourPhone, MainActivity.patientNumber);
		doctorName = settingPrefs.getString(DoctorName, MainActivity.doctorName);
		doctorEmail = settingPrefs.getString(DoctorEmail, MainActivity.doctorEmail);
		familyName = settingPrefs.getString(FamilyName, "");
		familyPhone = settingPrefs.getString(FamilyPhone, MainActivity.patientEmergency);
		
		MainActivity.patientName = yourName;
		MainActivity.patientNumber = yourPhone;
		MainActivity.doctorName = doctorName;
		MainActivity.doctorEmail = doctorEmail;
		MainActivity.patientEmergency = familyPhone;
		
		adapter.notifyDataSetChanged();
	}
	
	ExpandableListView.OnChildClickListener childClickListener = new OnChildClickListener() {
		
		@Override
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, final int childPosition, long id) {
			// TODO Auto-generated method stub
			if (groupPosition == 3) {
				AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
		        builder.setTitle("操作");
		        builder.setIcon(R.drawable.ic_launcher);
		        builder.setMessage("觀看或是上傳此歷史紀錄？");
		        builder.setNegativeButton("上傳", new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						UploadThread thread = new UploadThread(listFile[childPosition].getPath(), listFile[childPosition].getName());
						thread.start();
					}
				});
		        builder.setPositiveButton("觀看", new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						// TODO Auto-generated method stub
						Intent chartIntent = new Intent();
						chartIntent.setClass(SettingsActivity.this, ChartActivity.class);
						
						Bundle extras = new Bundle();
						extras.putSerializable("chartFile", listFile[childPosition]);
						
						chartIntent.putExtras(extras);
						startActivity(chartIntent);
					}
				});
		        builder.show();
			} else {
				String content = "";
				String tag = "";
	            if (groupPosition == 0) {
	            	if (childPosition == 0) {
	            		content = yourName;
	            		tag = YourName;
	            	} else {
	            		content = yourPhone;
	            		tag = YourPhone;
	            	}
	            }else if (groupPosition == 1) {
	            	if (childPosition == 0) {
	            		content = doctorName;
	            		tag = DoctorName;
	            	} else {
	            		content = doctorEmail;
	            		tag = DoctorEmail;
	            	}
	            } else if (groupPosition == 2) {
	            	if (childPosition == 0) {
	            		content = familyName;
	            		tag = FamilyName;
	            	} else {
	            		content = familyPhone;
	            		tag = FamilyPhone;
	            	}
	            }
	            
				FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
				Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
			    if (prev != null) {
			        ft.remove(prev);
			    }
			    ft.addToBackStack(null);
			    ContactDialogFragment cdf = ContactDialogFragment.newInstance(adapter.getChild(groupPosition, childPosition), content, tag);
				cdf.show(ft, "dialog");
			}
			return false;
		}
	};
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	class UploadThread extends Thread {
		
		String path;
		String name;
		
		public UploadThread(String path, String name) {
			this.path = path;
			this.name = name;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
			
			final HttpClient httpClient = new DefaultHttpClient();
		    final HttpContext localContext = new BasicHttpContext();
		    try {
		    			    	
			    final HttpPost httpPost = new HttpPost(MainActivity.mainUrl + "create_chart_file/" + 
						URLEncoder.encode(name, "UTF-8").replace("+", "%20") +"/"+ 
						URLEncoder.encode(Double.toString(MainActivity.location.getLongitude())+","+Double.toString(MainActivity.location.getLatitude()), "UTF-8") +"/"+ 
						MainActivity.doctorId +"/"+ 
						MainActivity.patientId +"/-1");

		    
		        final MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		        entity.addPart("userfile", new FileBody(new File (path)));
		        httpPost.setEntity(entity);

		        final HttpResponse response = httpClient.execute(httpPost, localContext);

		        final String responseBody = EntityUtils.toString(response.getEntity());
		        
		        Log.d(TAG, responseBody);
		    } catch (final Exception e) {
		        e.printStackTrace();
		    }
		}
	}
	
	public class ExpandableListAdapter extends BaseExpandableListAdapter {
        @Override
        public String getChild(int groupPosition, int childPosition) {
        	if (groupPosition == 3)
        		return listFile[childPosition].getName();
        	
            return Child[groupPosition][childPosition];
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
        	if (groupPosition == 3)
        		return listFile.length;
        	
            return Child[groupPosition].length;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                View convertView, ViewGroup parent) {
            View view = LayoutInflater.inflate(SettingsActivity.this,
                    R.layout.expandable_list_child_item);
            TextView textView = (TextView) view.findViewById(R.id.text_name);
            
            String text = "";
            if (groupPosition == 0) {
            	if (childPosition == 0) {
            		text = getChild(groupPosition, childPosition).toString() + yourName;
            	} else {
            		text = getChild(groupPosition, childPosition).toString() + yourPhone;
            	}
            }else if (groupPosition == 1) {
            	if (childPosition == 0) {
            		text = getChild(groupPosition, childPosition).toString() + doctorName;
            	} else {
            		text = getChild(groupPosition, childPosition).toString() + doctorEmail;
            	}
            } else if (groupPosition == 2) {
            	if (childPosition == 0) {
            		text = getChild(groupPosition, childPosition).toString() + familyName;
            	} else {
            		text = getChild(groupPosition, childPosition).toString() + familyPhone;
            	}
            } else {
            	text = getChild(groupPosition, childPosition).toString();
            }
            textView.setText(text);
            return view;
        }

        @Override
        public String getGroup(int groupPosition) {
            return Group[groupPosition];
        }

        @Override
        public int getGroupCount() {
            return Group.length;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                ViewGroup parent) {
            View view = LayoutInflater.inflate(SettingsActivity.this,
                    R.layout.expandable_list_group_item);
            TextView textView = (TextView) view.findViewById(R.id.text_name);
            ImageView imageIcon = (ImageView) view.findViewById(R.id.image_icon);
            
            textView.setText(getGroup(groupPosition).toString());
            
            if (groupPosition == 0) {
            	imageIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_list_phone));
            } else if (groupPosition == 1) {
            	imageIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_list_doctor));
            } else if (groupPosition == 2) {
            	imageIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_list_family));
            } else {
            	imageIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_list_history));
            }
            return view;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }
}
