package com.health.heartcare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.Switch;
import org.holoeverywhere.widget.Toast;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.text.format.Time;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.fima.chartview.ChartView;
import com.fima.chartview.LinearSeries;
import com.fima.chartview.LinearSeries.LinearPoint;
import com.health.heartcare.ValueLabelAdapter.LabelOrientation;

public class MainActivity extends SherlockActivity {
	
	public static String saveFolder = "PatchCare";
	public static String ip = "120.126.11.183"; 
	public static String mainUrl = "http://"+ip+"/index.php?/patch/";
	public static String patientName = "王伯伯";
	public static String patientGender = "1";
	public static String patientNumber = "0988123456";
	public static String patientEmergency = "0982555777";
	public static String doctorName = "王醫生";
	public static String doctorEmail = "doctor.wang@patchcare.com";
	public static String familyName;
	public static int httpTimeOut = 6500;
	public static boolean isSend = true;
	public static final int SHOW_NUMBER = 800;
	public static final int SEND_NUMBER = 100;
	public static final int REQUEST_ENABLE_BT = 0; 
	private static final UUID MY_UUID = UUID
			.fromString("00001101-0000-1000-8000-00805F9B34FB");
	
	ArrayList<String> listBTDevices;
	
	ArrayList<Double> listECGData;
	ArrayList<Double> listECGDataFilter;
	ArrayList<Long> listECGTime;
	
	ArrayList<Integer> listRPosition;
	ArrayList<Double> listRValue;
	ArrayList<Double> listRSave;
	
	ArrayList<Double> listResData;
	ArrayList<Double> listResDataFilter;
	ArrayList<Long> listResTime;
	ArrayList<Integer> listResPosition;
	ArrayList<Double> listResSave;
	
	double maxi = Double.MIN_VALUE;
	int rStartPosition = 0;
	double rStartValue = Double.MIN_VALUE;
	int resEndValue = 0;
	int resStartValue = 0;
	double slopeTh = 0;
	
	LinearSeries seriesECG;
	LinearSeries seriesRes;

	ChartView chartHeart;
	ChartView chartBreath;
	
	TextView textECG;
	TextView textRes;
	
	Switch switchSend;
	
	ProgressDialog progressDialog;
	
	LocationManager locationManager;
	public static Location location;
	String bestProvider = LocationManager.GPS_PROVIDER;
	
	BluetoothAdapter btAdapter = null;
	BluetoothSocket btSocket = null;
	InputStream btIn = null;
	
	GetIdTask getIdTask;
	ReceiveBTDataThread receiveBThread;
	ReadDataTask readDataTask;
//	ShowDataThread showDataThread;
	SaveDataThread saveDataThread;
	ExecutorService executorService;
	
	boolean isGPSStatusOK;
	boolean isStorageOK;
	boolean isBlueToothOK;
	boolean isConnected = false;
	boolean isSendMsg;
	
	String localPath;
	public static String patientId;
	public static String doctorId;
	
	UsefulClass useful = new UsefulClass();
	
	final String TAG = "MainActivity";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTheme(R.style.Holo_Theme_Light_DarkActionBar); // set the theme
		/* set screen always bright */
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		requestWindowFeature(Window.FEATURE_PROGRESS);

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
			requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.activity_main);
		
		getSherlock().setProgressBarIndeterminateVisibility(false);
		
		isSendMsg = false;
		
		/* set the gps */
		locationManager = (LocationManager) (this.getSystemService(Context.LOCATION_SERVICE));
		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			isGPSStatusOK = true;
			Criteria criteria = new Criteria();	//資訊提供者選取標準
			bestProvider = locationManager.getBestProvider(criteria, true);	// 選擇精準度最高的提供者
			location = locationManager.getLastKnownLocation(bestProvider); // 可能為 null
			
			if (location == null) {
				getLocation();
			}
		} else {
			isGPSStatusOK = false;
			Toast.makeText(this, "請檢查您的 GPS 裝置", Toast.LENGTH_LONG).show();
			startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		}
		
		/* set the bluetooth */
		listBTDevices = new ArrayList<String>();
		btAdapter = BluetoothAdapter.getDefaultAdapter();
		if (btAdapter == null) {
			Toast.makeText(this, "請確認您手機的藍芽裝置",
					Toast.LENGTH_LONG).show();
		} else {
			if (btAdapter.isEnabled()) {
				IntentFilter intentFilter = new IntentFilter();
				intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
				intentFilter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
				intentFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
				intentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
				intentFilter.addAction(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
				intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
				intentFilter.addAction(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED);
				registerReceiver(searchDevicesReceiver, intentFilter);
				
				btAdapter.cancelDiscovery();
				btAdapter.startDiscovery();
			} else {
				Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableBTIntent, REQUEST_ENABLE_BT);
			}
		}
		
		findViews();
		
		switchSend.setChecked(isSend);
		switchSend.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				isSend = isChecked;
			}
		});
		
		/* set action bar */
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowHomeEnabled(true);
		actionBar.setLogo(getResources().getDrawable(R.drawable.ic_main_logo));
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.ic_main_actionbar));
		
		TypedValue tv = new TypedValue();
		if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
		{
		    Log.i(TAG, "actionbar height:"+TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics()));
		}
		
		/* check the storage */
		if (useful.hasStorage(true)) {
			localPath = Environment.getExternalStorageDirectory().getPath();
			if (!useful.createFolder(localPath, saveFolder)) {
				localPath = null;
				Toast.makeText(
						this,
						"您的裝置沒有內存容量",
						Toast.LENGTH_LONG).show();
			}
		} else {
			localPath = null;
			Toast.makeText(
					this,
					"您的裝置沒有內存容量",
					Toast.LENGTH_LONG).show();
		}
		
		if (localPath != null) {
			isStorageOK = true;
		} else {
			isStorageOK = false;
		}
		
		/* read the data */
//		readDataTask = new ReadDataTask();
//		readDataTask.execute();
		
	}
	
	final int showECGData = 0;
	final int showECGRate = 1;
	final int showResData = 2;
	final int showResRate = 3;
	final int showSendMsg = 4;
	final int showToast = 5;
	final int showLocation = 6;
	final int sendSmsMsg = 7;
	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case showECGData:
				// add chart view with ecg data
				seriesECG = new LinearSeries();
				seriesECG.setLineColor(0xFFFF9100);
				seriesECG.setLineWidth(5);
				
				for (int i = msg.getData().getInt("ECG_DATA_START"); i < msg.getData().getInt("ECG_DATA_END"); i++) {	
					/* add 10 data */
					seriesECG.addPoint(new LinearPoint(i, listECGDataFilter.get(i)));
				}
				
				chartHeart.clearSeries();
				chartHeart.addSeries(seriesECG);
				chartHeart.setLeftLabelAdapter(new ValueLabelAdapter(MainActivity.this, LabelOrientation.VERTICAL));
				chartHeart.setBottomLabelAdapter(new ValueLabelAdapter(MainActivity.this, LabelOrientation.HORIZONTAL));
				break;
				
			case showECGRate:
		        textECG.setText(String.format("%1$.0f", Double.valueOf(msg.getData().getDouble("ECG_RATE"))));
				break;
				
			case showResData:
				// add chart view with respiration data
				seriesRes = new LinearSeries();
				seriesRes.setLineColor(0xFFFF9100);
				seriesRes.setLineWidth(5);
				
				for (int i = msg.getData().getInt("RES_DATA_START"); i < msg.getData().getInt("RES_DATA_END"); i++) {	
					/* add 10 data */
					seriesRes.addPoint(new LinearPoint(i, listResDataFilter.get(i)));
				}
				
				chartBreath.clearSeries();
				chartBreath.addSeries(seriesRes);
				chartBreath.setLeftLabelAdapter(new ValueLabelAdapter(MainActivity.this, LabelOrientation.VERTICAL));
				chartBreath.setBottomLabelAdapter(new ValueLabelAdapter(MainActivity.this, LabelOrientation.HORIZONTAL));
				break;
				
			case showResRate:
			     textRes.setText(String.format("%1$.0f", Double.valueOf(msg.getData().getDouble("RES_RATE"))));
				break;
				
			case showSendMsg:
				if (isSend) {
					Toast.makeText(
							MainActivity.this,
							"資訊已保存且傳送",
							Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(
							MainActivity.this,
							"資訊已保存但未傳送",
							Toast.LENGTH_LONG).show();
				}
				
				break;
				
			case showToast:
				Toast.makeText(
						MainActivity.this,
						msg.getData().getString("MESSAGE"),
						Toast.LENGTH_LONG).show();
				break;
				
			case showLocation:
				Toast.makeText(MainActivity.this, "目前位置："+location.getLatitude()+";"+location.getLongitude()+"。", Toast.LENGTH_SHORT).show();
				break;
				
			case sendSmsMsg:
				try {
//					double resData = msg.getData().getDouble("res_data");
//					double ecgData = msg.getData().getDouble("ecg_data");
					Date date = new Date();
		            DateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm");
		            df.format(date);
		            
//		            String message = message = patientName + "於 " +df.format(date)+ " 感到不適。" +
//							"心率："+String.format("%1$.0f", Double.valueOf(ecgData))
//							+"，呼吸率："+String.format("%1$.0f", Double.valueOf(resData))
//							+"，位置：" +
//							Double.toString(location.getLongitude())+","+Double.toString(location.getLatitude());
		            if (isGPSStatusOK)
		            	getLocation();
		            
		            String message = message = patientName + "於 " +df.format(date)+ " 感到不適。"
							+"，位置：" +
							Double.toString(location.getLongitude())+","+Double.toString(location.getLatitude());
							
					SmsManager smsManager = SmsManager.getDefault();
	
					List<String> texts = smsManager.divideMessage(message);
					for (String text : texts) {
						smsManager.sendTextMessage(patientEmergency, null, text, null, null);
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				break;
			}
			super.handleMessage(msg);
		}
	};
	
	private void findViews() {
		chartHeart = (ChartView) findViewById(R.id.chart_heart);
		chartBreath = (ChartView) findViewById(R.id.chart_breath);
		
		textECG = (TextView) findViewById(R.id.text_ecg);
		textRes = (TextView) findViewById(R.id.text_res);
		
		switchSend = (Switch) findViewById(R.id.send_switch);
	}
	
	public void getLocation() {
	    try {
	        // getting GPS status
	        boolean isGPSEnabled = locationManager
	                .isProviderEnabled(LocationManager.GPS_PROVIDER);

	        // getting network status
	        boolean isNetworkEnabled = locationManager
	                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

	        if (!isGPSEnabled && !isNetworkEnabled) {
	            // no network provider is enabled
	        } else {
	            if (isNetworkEnabled) {
	                locationManager.requestLocationUpdates(
	                        LocationManager.NETWORK_PROVIDER,
	                        1000,
	                        0, locationListener);
	                Log.d("Network", "Network Enabled");
	                if (locationManager != null) {
	                    location = locationManager
	                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
	                }
	            }
	            // if GPS Enabled get lat/long using GPS Services
	            if (isGPSEnabled) {
	                if (location == null) {
	                    locationManager.requestLocationUpdates(
	                            LocationManager.GPS_PROVIDER,
	                            1000,
	                            0, locationListener);
	                    Log.d("GPS", "GPS Enabled");
	                    if (locationManager != null) {
	                        location = locationManager
	                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
	                    }
	                }
	            }
	        }
	        
	        Message msg = new Message();
	        msg.what = showLocation;
	        mHandler.sendMessage(msg);

	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
//		getSupportMenuInflater().inflate(R.menu.main, menu);
		menu.add(0, 0, 0, "Data")
			.setIcon(R.drawable.ic_menu_setting)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		
		menu.add(0, 1, 1, "Change IP")
			.setIcon(R.drawable.ic_menu_ip)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		
		menu.add(0, 2, 2, "Device")
			.setIcon(R.drawable.ic_menu_bluetooth)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			break;
			
		case 0:
			Intent settingsIntent = new Intent();
			settingsIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			settingsIntent.setClass(MainActivity.this, SettingsActivity.class);
			startActivity(settingsIntent);
			break;
			
		case 1:
//			btAdapter.cancelDiscovery();
//			btAdapter.startDiscovery();
			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
	        builder.setTitle("更改 IP");
	        builder.setIcon(R.drawable.ic_launcher);
	        View view = View.inflate(MainActivity.this,
					R.layout.dialog_contact, null);
	        final TextView textSubject = (TextView) view.findViewById(R.id.text_subject);
	        final EditText editContent = (EditText) view.findViewById(R.id.edit_content);
	        editContent.setText(ip);
	        builder.setView(view);
	        builder.setNegativeButton(android.R.string.cancel, null);
	        builder.setPositiveButton(android.R.string.ok, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					// TODO Auto-generated method stub
					ip = editContent.getText().toString();
					mainUrl = "http://"+ip+"/index.php?/patch/";
					
					if (getIdTask == null || !getIdTask.getStatus().equals(AsyncTask.Status.RUNNING)) {
						getIdTask = new GetIdTask();
						getIdTask.execute();
					}
				}
			});
	        builder.show();
			break;
			
		case 2:
			btAdapter.cancelDiscovery();
			btAdapter.startDiscovery();
			builder = new AlertDialog.Builder(MainActivity.this);
			builder.setTitle("選擇裝置");
			builder.setItems((String [])(listBTDevices.toArray((new String[0]))), new DialogInterface.OnClickListener() {
				

				@Override
				public void onClick(DialogInterface arg0, final int position) {
					// TODO Auto-generated method stub
					final String selectedDeviceAddress = listBTDevices.get(position)
							.split("\\|")[1];
					
					if (isConnected)
						return;
					
					Runnable runnable = new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							try {
								btSocket = btAdapter.getRemoteDevice(selectedDeviceAddress)
										.createRfcommSocketToServiceRecord(MY_UUID);
								btSocket.connect();

								synchronized (MainActivity.this) {
									if (isConnected)
										return;
									isConnected = true;
									btIn = btSocket.getInputStream();
								}
								
								Message msg = new Message();
								msg.what = showToast;
								
								Bundle data = new Bundle();
								data.putString("MESSAGE", "裝置 "+ listBTDevices.get(position).split("\\|")[0] +" 已連接");
								
								msg.setData(data);
								mHandler.sendMessage(msg);
								
								if (receiveBThread == null || !receiveBThread.isRunning) {
									receiveBThread = new ReceiveBTDataThread();
									receiveBThread.start();
								}
							} catch (IOException e) {
								e.printStackTrace();

								isConnected = false;
								try {
									btSocket.close();
								} catch (Exception e2) {
									e2.printStackTrace();
								}

								btSocket = null;
								Message msg = new Message();
								msg.what = showToast;
								
								Bundle data = new Bundle();
								data.putString("MESSAGE", "請檢查藍芽裝置");
								
								msg.setData(data);
								mHandler.sendMessage(msg);
							}
						}
					};

					Thread thread = new Thread(runnable);
					thread.start();
					
				}
				
			});
			builder.show();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		isSend = true;
		switchSend.setChecked(isSend);
		
		/* set the prefs */
		SharedPreferences settingPrefs = getSharedPreferences(SettingsActivity.Settings, Context.MODE_PRIVATE);
		patientName = settingPrefs.getString(SettingsActivity.YourName, MainActivity.patientName);
		patientNumber = settingPrefs.getString(SettingsActivity.YourPhone, MainActivity.patientNumber);
		doctorName = settingPrefs.getString(SettingsActivity.DoctorName, MainActivity.doctorName);
		doctorEmail = settingPrefs.getString(SettingsActivity.DoctorEmail, MainActivity.doctorEmail);
		familyName = settingPrefs.getString(SettingsActivity.FamilyName, "");
		patientEmergency = settingPrefs.getString(SettingsActivity.FamilyPhone, MainActivity.patientEmergency);
		
		if (getIdTask == null || !getIdTask.getStatus().equals(AsyncTask.Status.RUNNING)) {
			getIdTask = new GetIdTask();
			getIdTask.execute();
		}
		
		if (isGPSStatusOK) {
			getLocation();
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
//		if (readDataTask != null && readDataTask.getStatus().equals(AsyncTask.Status.RUNNING)) {
//			readDataTask.cancel(true);
//		}
		
//		if (showDataThread != null && showDataThread.isAlive()) {
//			showDataThread.stopThread();
//			showDataThread.interrupt();
//		}
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(searchDevicesReceiver);
		
		if (receiveBThread != null)
			receiveBThread.stopThread();

		if (btIn != null) {
			try {
				btSocket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
		case REQUEST_ENABLE_BT:
			break;
		}
	}

	LocationListener locationListener = new LocationListener() {
	    public void onLocationChanged(Location location) {
	      // Called when a new location is found by the network location provider.
	    	MainActivity.this.location = location;
	    }

	    public void onStatusChanged(String provider, int status, Bundle extras) {}

	    public void onProviderEnabled(String provider) {}

	    public void onProviderDisabled(String provider) {}
	};
	
	public class ReceiveBTDataThread extends Thread {
		
		public int count, showCount, intShownRes;
		public boolean isRunning;
		public boolean isCreateInServer;
		public boolean isCreateInLocal;

		File recordFile = null;
		FileOutputStream outputStream = null;
		String recordFileName;
		
		double firstRValue = Double.MIN_VALUE;
		int firstRPosition = 0;
		
		final String TAG = "ReceiveBTDataThread";
		
		public ReceiveBTDataThread() {
			count = 0;
			intShownRes = 0;
			showCount = SHOW_NUMBER;
			isRunning = true;
			isCreateInServer = true;
			isCreateInLocal = true;
			
			executorService = Executors.newFixedThreadPool(10);
		}
		
		public void stopThread() {
			isRunning = false;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
			
			listECGData = new ArrayList<Double>();
			listECGDataFilter = new ArrayList<Double>();
			listResData = new ArrayList<Double>();
			listResDataFilter = new ArrayList<Double>();
			listECGTime = new ArrayList<Long>();
			listResTime = new ArrayList<Long>();
			
			listRPosition = new ArrayList<Integer>();
			listRValue = new ArrayList<Double>();
			listRSave = new ArrayList<Double>();
			listResPosition = new ArrayList<Integer>();
			listResSave = new ArrayList<Double>();
            
			byte[] arrECG = new byte[3];
			byte[] arrRes = new byte[3]; 
			// get data from bt device
			while (isRunning) {
				try {
					int bytesAvailable = btIn.available();
					
					if (bytesAvailable > 0) {
						byte[] packetBytes = new byte[bytesAvailable];
						btIn.read(packetBytes);
						for (int i = 0; i < bytesAvailable; i++) {
							byte b = packetBytes[i];
							// 跳過起始的
							if (count > 5) {
								switch (count % 6) {
								case 0:
									if (count/6 > 1) {
										if (listECGData.isEmpty() && listResData.isEmpty() && 
												listECGTime.isEmpty() && listResTime.isEmpty() &&
												listRSave.isEmpty() && listResSave.isEmpty() && 
												listECGDataFilter.isEmpty() && listResDataFilter.isEmpty()) {
											if (isGPSStatusOK) {
												getLocation();
											}
											/* sene sms message */
											if (!isSendMsg) {
												Message msg = new Message();
												msg.what = sendSmsMsg;
												mHandler.sendMessage(msg);
												isSendMsg = true;
											}
											/* get first data and create file  */
											Date date = new Date();
								            DateFormat df = new SimpleDateFormat("yyyy-MM-dd_kk_mm");
								            recordFileName = df.format(date) + ".csv";
								            // create in local
								            recordFile = new File(localPath +File.separator+ saveFolder +File.separator+ recordFileName);
								            if (!recordFile.exists()) {
								            	try {
													recordFile.createNewFile();
												} catch (IOException e) {
													// TODO Auto-generated catch block
													isCreateInLocal = false;
													Log.e(TAG, "create in local error: "+e.toString());
												}
								            }
								            
								            try {
												outputStream =  new FileOutputStream(recordFile, true);
												String data = "ECG,RES,ECG_TIME,RES_TIME,R_VALUE,RES_VALUE\n";
												outputStream.write(data.getBytes());
											} catch (Exception e) {
												// TODO Auto-generated catch block
												isCreateInLocal = false;
												Log.e(TAG, "insert first line in local error: "+e.toString());
											}
								            
								            // create in the server
								            if (isSend && doctorId != null && patientId != null) {
									            try {
													final HttpParams httpParameters = new BasicHttpParams();
													// Set the timeout in milliseconds until a connection is established.
													HttpConnectionParams.setConnectionTimeout(httpParameters, httpTimeOut);
									
													// Set the default socket timeout (SO_TIMEOUT)
													// in milliseconds which is the timeout for waiting for data.
													HttpConnectionParams.setSoTimeout(httpParameters, httpTimeOut);
									
													final HttpClient client = new DefaultHttpClient(httpParameters);
													
													double longitude = 0;
													double latitude = 0;
													if (location != null) {
														longitude = location.getLongitude();
														latitude = location.getLatitude();
													}
													
													Log.i(TAG, mainUrl + "create_chart_file/" + 
															URLEncoder.encode(recordFileName, "UTF-8").replace("+", "%20") +"/"+ 
															URLEncoder.encode(Double.toString(longitude)+","+Double.toString(latitude), "UTF-8") +"/"+ 
															doctorId +"/"+ 
															patientId +"/0");
													
													final HttpGet request = new HttpGet(mainUrl + "create_chart_file/" + 
															URLEncoder.encode(recordFileName, "UTF-8").replace("+", "%20") +"/"+ 
															URLEncoder.encode(Double.toString(longitude)+","+Double.toString(latitude), "UTF-8") +"/"+ 
															doctorId +"/"+ 
															patientId +"/0");
													
													final HttpResponse response = client.execute(request);
									
													if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
														String responseContent = EntityUtils.toString(response.getEntity());
														
														if (responseContent.equals("successed")) {
															
														} else {
															isCreateInServer = false;
														}
														Log.d(TAG, "response content: " + responseContent);
													} else {
														isCreateInServer = false;
													}
												} catch (Exception e) {
													Log.e(TAG, "create in server error: "+e.toString());
													isCreateInServer = false;
												}
								            } else {
								            	isCreateInServer = false;
								            }
										}
										long time = System.currentTimeMillis();
										
										String strECG = "";
										String strRes = "";
										for (int j = 0; j < arrECG.length; j++) {
											strECG += String.format("%8s", Integer.toBinaryString(arrECG[j] & 0xFF)).replace(' ', '0');
											strRes += String.format("%8s", Integer.toBinaryString(arrRes[j] & 0xFF)).replace(' ', '0');
										}
										listECGData.add((double)Long.parseLong(strECG, 2));
										listECGDataFilter.add((double)Long.parseLong(strECG, 2));
										listResData.add((double)Long.parseLong(strRes, 2));
										listResDataFilter.add((double)Long.parseLong(strRes, 2));
										
										listECGTime.add(time);
										listResTime.add(time);
										
										listRSave.add(0.00);
										listResSave.add(0.00);
										
										/* coefficient */
										if (listECGData.size() > FilterCoefficient.ECG_COEFF.length) {
											double ecgValue = 0;
											for (int j = 0; j < FilterCoefficient.ECG_COEFF.length; j++) {
												ecgValue += listECGData.get(listECGData.size()-FilterCoefficient.ECG_COEFF.length+j)*FilterCoefficient.ECG_COEFF[j];
											}
											
											listECGDataFilter.set(listECGDataFilter.size()-1, ecgValue);	
										}
										if (listResData.size() > FilterCoefficient.RES_COEFF.length) {
											double resValue = 0;
											for (int j = 0; j < FilterCoefficient.RES_COEFF.length; j++) {
												resValue += listResData.get(listResData.size()-FilterCoefficient.RES_COEFF.length+j)*FilterCoefficient.RES_COEFF[j];
											}
											
											listResDataFilter.set(listResDataFilter.size()-1, resValue);
										}
										
										/* calculate the ecg rate */
										if (listECGData.size() >= 502) {
											double slope = -2*listECGDataFilter.get(listECGDataFilter.size()-5)-listECGDataFilter.get(listECGDataFilter.size()-4)
													+listECGDataFilter.get(listECGDataFilter.size()-2)+2*listECGDataFilter.get(listECGDataFilter.size()-1);
											if (listECGDataFilter.size() <= 502+120) {
												if (slope > maxi) {
													maxi = slope;
													slopeTh = 0.5*maxi;
												}
											} else {
												double slope_1 = -2*listECGDataFilter.get(listECGDataFilter.size()-6)-listECGDataFilter.get(listECGDataFilter.size()-5)
														+listECGDataFilter.get(listECGDataFilter.size()-3)+2*listECGDataFilter.get(listECGDataFilter.size()-2);
												
												if (listRPosition.isEmpty()) {
													// first R
													if (slope > slopeTh && slope_1 > slopeTh && firstRPosition == 0) {
														rStartPosition = listECGDataFilter.size()-3;
														rStartValue = listECGDataFilter.get(listECGDataFilter.size()-3);
														
														firstRPosition = listECGDataFilter.size()-3;
														firstRValue = listECGDataFilter.get(listECGDataFilter.size()-3);
													}
													
													if (firstRPosition != 0) {
														if (listECGData.size()-3 < rStartPosition+20) {
															if (listECGDataFilter.get(listECGDataFilter.size()-3) > firstRValue) {
																firstRPosition = listECGDataFilter.size()-3;
																firstRValue = listECGDataFilter.get(listECGDataFilter.size()-3);
															}
														} else {
															listRPosition.add(firstRPosition);
															listRValue.add(firstRValue);
															
															maxi = ((listRValue.get(0)-rStartValue)-maxi)/16+maxi;
															slopeTh = 0.5*maxi;
															
															firstRPosition = 0;
															firstRValue = Double.MIN_VALUE;
														}
													}
												} else {
													// update maxi
													if (listECGData.size()-3 > listRPosition.get(listRPosition.size()-1)+60) {
														if (slope > slopeTh && slope_1 > slopeTh && firstRPosition == 0) {
															rStartPosition = listECGDataFilter.size()-3;
															rStartValue = listECGDataFilter.get(listECGDataFilter.size()-3);
															
															firstRPosition = listECGDataFilter.size()-3;
															firstRValue = listECGDataFilter.get(listECGDataFilter.size()-3);
														} else if (firstRPosition == 0 
																&& (listECGTime.get(listECGTime.size()-1)-listECGTime.get(listRPosition.get(listRPosition.size()-1)+60) > 2000)) {
															// reset threshold to debug
															double slopeReset = -2*listECGDataFilter.get(listECGDataFilter.size()-5)-listECGDataFilter.get(listECGDataFilter.size()-4)
																	+listECGDataFilter.get(listECGDataFilter.size()-2)+2*listECGDataFilter.get(listECGDataFilter.size()-1);
															maxi = slopeReset;
															slopeTh = 0.5*maxi;
														}
														
														if (firstRPosition != 0) {
															if (listECGDataFilter.size()-3 < rStartPosition+20) {
																if (listECGDataFilter.get(listECGDataFilter.size()-3) > firstRValue) {
																	firstRPosition = listECGDataFilter.size()-3;
																	firstRValue = listECGDataFilter.get(listECGDataFilter.size()-3);
																}
															} else {
																listRPosition.add(firstRPosition);
																listRValue.add(firstRValue);
																
																maxi = ((listRValue.get(listRValue.size()-1)-rStartValue)-maxi)/16+maxi;
																slopeTh = 0.5*maxi;
																
																firstRPosition = 0;
																firstRValue = Double.MIN_VALUE;
															}
														}
													}
												}
											}
											
											if (listRPosition.size() > 1) {
												
												double diffTime = (double)(listECGTime.get(listRPosition.get(listRPosition.size()-1))-
														listECGTime.get(listRPosition.get(listRPosition.size()-2))) / 1000;
												
//												Log.d(TAG, "ecg rate:"+diffTime+";"+listRPosition.get(listRPosition.size()-1)+";"
//												+listRPosition.get(listRPosition.size()-2)+";"+listECGTime.get(listRPosition.get(listRPosition.size()-1))+";"+
//												listECGTime.get(listRPosition.get(listRPosition.size()-2)));
												
												if (diffTime != 0) {
													double rate = 60/diffTime;
													if (rate > 150 || (rate < 40 && rate >= 10)) {
														rate = 99;
													} else if (rate < 10 && rate >= 0) {
													}
													Message msg = new Message();
													msg.what = showECGRate;
													
													Bundle data = new Bundle();
													data.putDouble("ECG_RATE", 
															rate);
													msg.setData(data);
													
													mHandler.sendMessage(msg);
													
													listRSave.set(listECGData.size()-1, rate);
												}
											}
										}
										
										/* calculate the res rate */
										if (listResData.size() >= 201) {
											if ((listResDataFilter.get(listResDataFilter.size()-1) > listResDataFilter.get(listResDataFilter.size()-2)) && resStartValue == 0) {
												resStartValue = listResDataFilter.size()-1;
											}
											
											if (resStartValue != 0) {
												if (listResDataFilter.get(listResDataFilter.size()-2) >= listResDataFilter.get(listResDataFilter.size()-1)) {
													resEndValue = listResDataFilter.size()-2;
												}
											}
											
											if (resStartValue != 0 && resEndValue != 0) {
												if (Math.abs(listResDataFilter.get(resStartValue)-listResDataFilter.get(resEndValue)) > 1000) {
													listResPosition.add(resEndValue);
													
													resStartValue = 0;
													resEndValue = 0;
												} else {
													resEndValue = 0;
												}
											}
											
											if (listResPosition.size() > 1) {
												double diffTime = (double)(listResTime.get(listResPosition.get(listResPosition.size()-1))-listResTime.get(listResPosition.get(listResPosition.size()-2))) / 1000;
												
//												Log.d(TAG, "res rate:"+diffTime+";"+listResPosition.get(listResPosition.size()-1)+";"
//														+listResPosition.get(listResPosition.size()-2)+";"+listResTime.get(listResPosition.get(listResPosition.size()-1))+";"+
//														listResTime.get(listResPosition.get(listResPosition.size()-2)));
												if (intShownRes != 0 && listResPosition.size() == intShownRes && (listResTime.get(listResTime.size()-1)-listResTime.get(listResPosition.get(intShownRes-1)) > 8000)) {
													Message msg = new Message();
													msg.what = showResRate;
													
													Bundle data = new Bundle();
													data.putDouble("RES_RATE", 0);
													
													msg.setData(data);
													mHandler.sendMessage(msg);
													
													listResSave.set(listResData.size()-1, 0d);
												} else if (diffTime != 0) {
													double rate = 60/diffTime;
													
													if (rate >= 30) {
														rate = 18;
													}
													
													Message msg = new Message();
													msg.what = showResRate;
													
													Bundle data = new Bundle();
													data.putDouble("RES_RATE", rate);
													
													msg.setData(data);
													mHandler.sendMessage(msg);
													
													listResSave.set(listResData.size()-1, rate);
													intShownRes = listResPosition.size();
												}
											}
										}
										
										/* show the ecg data */
										if (listECGDataFilter.size() >= showCount) {
											if (listECGDataFilter.size() % 5 == 0) {
												/* show ecg data */
												Message msg = new Message();
												msg.what = showECGData;
												
												Bundle data = new Bundle();
												data.putInt("ECG_DATA_START", listECGDataFilter.size()-showCount);
												data.putInt("ECG_DATA_END", listECGDataFilter.size());
												
												msg.setData(data);
												mHandler.sendMessage(msg);
												
											}
//											if (showDataThread == null || !showDataThread.running) {
//												showDataThread = new ShowDataThread();
//												showDataThread.start();
//											}
//											
//											if (localPath != null && (saveDataThread == null || !saveDataThread.running)) {
//												saveDataThread = new SaveDataThread(listECGData, listECGTime, listResData, listResTime, listRSave, listResSave);
//												saveDataThread.start();
//											}
										}
										/* show the res data */
										if (listResDataFilter.size() >= showCount) {
											if (listResDataFilter.size() % 5 == 0) {
												/* show respiration data */
												Message msg = new Message();
												msg.what = showResData;
												
												Bundle data = new Bundle();
												data.putInt("RES_DATA_START", listResDataFilter.size()-showCount);
												data.putInt("RES_DATA_END", listResDataFilter.size());
												
												msg.setData(data);
												mHandler.sendMessage(msg);
											}
										}
										
										/* save data in local */
										if (isCreateInLocal) {
											if (outputStream != null) {
												String data = Double.toString(listECGDataFilter.get(listECGDataFilter.size()-1)) +","+ Double.toString(listResDataFilter.get(listResDataFilter.size()-1))
								            			+","+ Double.toString(listECGTime.get(listECGTime.size()-1)) +","+ Double.toString(listResTime.get(listResTime.size()-1)) 
								            			+","+ Double.toString(listRSave.get(listResSave.size()-1)) +","+ Double.toString(listResSave.get(listResSave.size()-1)) +"\n";
												
												try {
													outputStream.write(data.getBytes());
												} catch (IOException e) {
													// TODO Auto-generated catch block
													Log.d(TAG, "save data in local error: "+e.toString());
												}
											}
										}
										
										/* save data in server */
						            	if (isCreateInServer && isSend) {
						            		if (!listECGDataFilter.isEmpty()) {
							            		if (listECGDataFilter.size() % SEND_NUMBER == 0) {
							            			JSONArray jsonArr = new JSONArray();
							            			for (int j = ((listECGDataFilter.size()/SEND_NUMBER)-1)*SEND_NUMBER; j < (listECGDataFilter.size()/SEND_NUMBER)*SEND_NUMBER; j++) {
							            				JSONObject jsonObj = new JSONObject();
							            				jsonObj.put("ECG", listECGDataFilter.get(j));
							            				jsonObj.put("RES", listResDataFilter.get(j));
							            				jsonObj.put("ECG_TIME", listECGTime.get(j));
							            				jsonObj.put("RES_TIME", listResTime.get(j));
							            				jsonObj.put("R_VALUE", listRSave.get(j));
							            				jsonObj.put("RES_VALUE", listResSave.get(j));
							            				
							            				jsonArr.put(jsonObj);
							            			}
							            			
							            			final JSONArray jsonArrSave = jsonArr;
							            			final String fileNameSave = recordFileName;
							            			final int countNum = listECGDataFilter.size();
							            			
							            			Runnable runnable = new Runnable() {
														@Override
														public void run() {
															// TODO Auto-generated method stub
															try {
											    				final HttpParams httpParameters = new BasicHttpParams();
											    				// Set the timeout in milliseconds until a connection is established.
											    				HttpConnectionParams.setConnectionTimeout(httpParameters, httpTimeOut);
										
											    				// Set the default socket timeout (SO_TIMEOUT)
											    				// in milliseconds which is the timeout for waiting for data.
											    				HttpConnectionParams.setSoTimeout(httpParameters, httpTimeOut);
										
											    				final HttpClient client = new DefaultHttpClient(httpParameters);
											    				
											    				final HttpPost request = new HttpPost(mainUrl + "update_chart_file_by_json/" + 
											    						URLEncoder.encode(fileNameSave, "UTF-8").replace("+", "%20") +"/"+
											    						countNum);
//											    				request.setHeader("Content-type", "application/json");
											    				
											    				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();  
											    				nameValuePairs.add(new BasicNameValuePair("data", jsonArrSave.toString()));  
											    				request.setEntity(new UrlEncodedFormEntity(nameValuePairs));  
											    				
//											    				request.setEntity(new StringEntity(jsonArrSave.toString(), "UTF-8"));
											    				
											    				final HttpResponse response = client.execute(request);
										
											    				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
											    					String responseContent = EntityUtils.toString(response.getEntity());
											    					Log.i(TAG, "reponse:"+responseContent+";"+countNum);
											    					if (responseContent.equals("successed")) {
											    						
											    					} else {
											    						// save error
											    					}
											    				} else {
											    					// save error
											    				}
											    			} catch (Exception e) {
											    				Log.e(TAG, "save data in server error: "+e.toString());
											    			}
														}
													};
													
													executorService.submit(runnable);
							            		} else if (listECGDataFilter.size() == 3748) {
							            			JSONArray jsonArr = new JSONArray();
							            			for (int j = 3700; j < 3748; j++) {
							            				JSONObject jsonObj = new JSONObject();
							            				jsonObj.put("ECG", listECGDataFilter.get(j));
							            				jsonObj.put("RES", listResDataFilter.get(j));
							            				jsonObj.put("ECG_TIME", listECGTime.get(j));
							            				jsonObj.put("RES_TIME", listResTime.get(j));
							            				jsonObj.put("R_VALUE", listRSave.get(j));
							            				jsonObj.put("RES_VALUE", listResSave.get(j));
							            				
							            				jsonArr.put(jsonObj);
							            			}
							            			
							            			final JSONArray jsonArrSave = jsonArr;
							            			final String fileNameSave = recordFileName;
							            			final int countNum = listECGData.size();
							            			
							            			Runnable runnable = new Runnable() {
														@Override
														public void run() {
															// TODO Auto-generated method stub
															try {
											    				final HttpParams httpParameters = new BasicHttpParams();
											    				// Set the timeout in milliseconds until a connection is established.
											    				HttpConnectionParams.setConnectionTimeout(httpParameters, httpTimeOut);
										
											    				// Set the default socket timeout (SO_TIMEOUT)
											    				// in milliseconds which is the timeout for waiting for data.
											    				HttpConnectionParams.setSoTimeout(httpParameters, httpTimeOut);
										
											    				final HttpClient client = new DefaultHttpClient(httpParameters);
											    				
											    				final HttpPost request = new HttpPost(mainUrl + "update_chart_file_by_json/" + 
											    						URLEncoder.encode(fileNameSave, "UTF-8").replace("+", "%20") +"/"+
											    						countNum);
											    				
											    				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();  
											    				nameValuePairs.add(new BasicNameValuePair("data", jsonArrSave.toString()));  
											    				request.setEntity(new UrlEncodedFormEntity(nameValuePairs)); 
											    				
//											    				request.setEntity(new StringEntity(jsonArrSave.toString(), "UTF-8"));
											    				
											    				final HttpResponse response = client.execute(request);
										
											    				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
											    					String responseContent = EntityUtils.toString(response.getEntity());
											    					Log.i(TAG, "reponse:"+responseContent+";"+countNum);
											    					if (responseContent.equals("successed")) {
											    						
											    					} else {
											    						// save error
											    					}
											    				} else {
											    					// save error
											    				}
											    			} catch (Exception e) {
											    				Log.e(TAG, "save data in server error: "+e.toString());
											    			}
														}
													};
													
													executorService.submit(runnable);
							            		}
						            		}
//						            		final String fileName = recordFileName;
//						            		final double ecgData = listECGData.get(listECGData.size()-1);
//						            		final double resDate = listResData.get(listResData.size()-1);
//						            		final long ecgTime = listECGTime.get(listECGTime.size()-1);
//						            		final long resTime = listResTime.get(listResTime.size()-1);
//						            		final double rSave = listRSave.get(listRSave.size()-1);
//						            		final double resSave = listResSave.get(listResSave.size()-1);
//						            		final int countNum = (listECGData.size()-1);
//						            		Runnable runnable = new Runnable() {
//												@Override
//												public void run() {
//													// TODO Auto-generated method stub
//													try {
//									    				final HttpParams httpParameters = new BasicHttpParams();
//									    				// Set the timeout in milliseconds until a connection is established.
//									    				HttpConnectionParams.setConnectionTimeout(httpParameters, httpTimeOut);
//								
//									    				// Set the default socket timeout (SO_TIMEOUT)
//									    				// in milliseconds which is the timeout for waiting for data.
//									    				HttpConnectionParams.setSoTimeout(httpParameters, httpTimeOut);
//								
//									    				final HttpClient client = new DefaultHttpClient(httpParameters);
//									    				
//									    				final HttpGet request = new HttpGet(mainUrl + "update_chart_file/" + 
//									    						URLEncoder.encode(fileName, "UTF-8").replace("+", "%20") +"/"+ 
//									    						URLEncoder.encode(Double.toString(ecgData), "UTF-8") +"/"+ 
//									    						URLEncoder.encode(Double.toString(resDate), "UTF-8")+"/"+
//									    						URLEncoder.encode(Long.toString(ecgTime), "UTF-8")+"/"+
//									    						URLEncoder.encode(Long.toString(resTime), "UTF-8")+"/"+
//									    						URLEncoder.encode(Double.toString(rSave), "UTF-8")+"/"+
//									    						URLEncoder.encode(Double.toString(resSave), "UTF-8")+"/"+
//									    						countNum);
//									    				
//									    				final HttpResponse response = client.execute(request);
//								
//									    				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
//									    					String responseContent = EntityUtils.toString(response.getEntity());
//									    					if (responseContent.equals("successed")) {
//									    						
//									    					} else {
//									    						// save error
//									    					}
//									    				} else {
//									    					// save error
//									    				}
//									    			} catch (Exception e) {
//									    				Log.e(TAG, "save data in server error: "+e.toString());
//									    			}
//												}
//											};
//											
//											executorService.submit(runnable);
						            	}
									}
									arrRes[0] = b;
									break;
								case 1:
									arrRes[1] = b;
									break;
								case 2:
									arrRes[2] = b;
									break;
								case 3:
									arrECG[0] = b;
									break;
								case 4:
									arrECG[1] = b;
									break;
								case 5:
									arrECG[2] = b;
									break;
								}
							} else {
								Log.d(TAG, "Receive data:"+String.format("%8s", Integer.toBinaryString(b & 0xFF)).replace(' ', '0')+":"+bytesAvailable);
							}
							count++;
						}
					} else {
						// no data
						if (count >= 22500 && 
								!listECGData.isEmpty() && !listResData.isEmpty() &&
								!listECGTime.isEmpty() && !listResTime.isEmpty() &&
								!listRSave.isEmpty() && !listResSave.isEmpty() &&
								!listECGDataFilter.isEmpty() && !listResDataFilter.isEmpty())  {
							// initialize
							Log.i(TAG, "NO DATA:"+count+";"+listECGData.size()+";"+listResData.size());
							
							/* close file in local */
							if (isCreateInLocal) {
								if (outputStream != null) {
									try {
										outputStream.close();
									} catch (IOException e) {
										// TODO Auto-generated catch block
										Log.e(TAG, "close outputstream in local error: "+e.toString());
									}
								}
							}
							
							/* close file in server */
							if (isCreateInServer) {
								final String fileName = recordFileName;
								Runnable runnable = new Runnable() {
									
									@Override
									public void run() {
										// TODO Auto-generated method stub
										try {
						    				final HttpParams httpParameters = new BasicHttpParams();
						    				// Set the timeout in milliseconds until a connection is established.
						    				HttpConnectionParams.setConnectionTimeout(httpParameters, httpTimeOut);
					
						    				// Set the default socket timeout (SO_TIMEOUT)
						    				// in milliseconds which is the timeout for waiting for data.
						    				HttpConnectionParams.setSoTimeout(httpParameters, httpTimeOut);
					
						    				final HttpClient client = new DefaultHttpClient(httpParameters);
						    				
						    				final HttpGet request = new HttpGet(mainUrl + "change_chart_file_status/" + 
						    						URLEncoder.encode(fileName, "UTF-8").replace("+", "%20") +"/"+ 
						    						"-1");
						    				
						    				final HttpResponse response = client.execute(request);
					
						    				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
						    					String responseContent = EntityUtils.toString(response.getEntity());
						    				} else {
						    					// close error
						    				}
						    			} catch (Exception e) {
						    				Log.e(TAG, "close file in server error: "+e.toString());
						    			}
									}
								};
								
								executorService.submit(runnable);
							}
							
							/* show save message */
							Message msg = new Message();
							msg.what = showSendMsg;
							mHandler.sendMessage(msg);
							
							Thread.sleep(1000);
							
							count = 0;
							resStartValue = 0;
							resEndValue = 0;
							firstRValue = Double.MIN_VALUE;
							firstRPosition = 0;
							rStartPosition = 0;
							rStartValue = Double.MIN_VALUE;
							slopeTh = 0;
							intShownRes = 0;
							maxi = Double.MIN_VALUE;
							
							listECGData = new ArrayList<Double>();
							listECGDataFilter = new ArrayList<Double>();
							listResData = new ArrayList<Double>();
							listResDataFilter = new ArrayList<Double>();
							listECGTime = new ArrayList<Long>();
							listResTime = new ArrayList<Long>();
							
							listRPosition = new ArrayList<Integer>();
							listRValue = new ArrayList<Double>();
							listResPosition = new ArrayList<Integer>();
							listRSave = new ArrayList<Double>();
							listResSave = new ArrayList<Double>();
							
							isCreateInServer = true;
							isCreateInLocal = true;
							isSendMsg = false;
						}
					}
				} catch (Exception e) {
					Log.e(TAG, "receive bt data error");
				}
			}
		}
		
	}
	
	public class SaveDataThread extends Thread {
		int count;
		boolean running;
		boolean isCreateFileInServerOK;
		boolean isUpdateDataInServerOK;
		
		public ArrayList<Double> listECGData;
		public ArrayList<Long> listECGTime;
		
		public ArrayList<Integer> listRPosition;
		
		public ArrayList<Double> listResData;
		public ArrayList<Long> listResTime;
		public ArrayList<Integer> listResPosition;
		
		public ArrayList<Double> listRSave;
		public ArrayList<Double> listResSave;
		
		public SaveDataThread(ArrayList<Double> listECGData, ArrayList<Long> listECGTime, 
				ArrayList<Double> listResData, ArrayList<Long> listResTime,
				ArrayList<Double> listRSave, ArrayList<Double> listResSave) {
			count = 0;
			isCreateFileInServerOK = true;
			executorService = Executors.newFixedThreadPool(1);
			
			this.listECGData = listECGData;
			this.listECGTime = listECGTime;
			this.listResData = listResData;
			this.listResTime = listResTime;
			this.listRSave = listRSave;
			this.listResSave = listResSave;
		}
		
		public void stopThread() {
			running = false;
		}
		
		public void updateData(ArrayList<Double> listECGData, ArrayList<Long> listECGTime, 
				ArrayList<Double> listResData, ArrayList<Long> listResTime,
				ArrayList<Double> listRSave, ArrayList<Double> listResSave) {
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
			running = true;
			
			Date date = new Date();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd_kk_mm");
            final String recordFileName = df.format(date) + ".csv";
            // create in local
            File recordFile = new File(localPath +File.separator+ saveFolder +File.separator+ recordFileName);
            if (!recordFile.exists()) {
            	try {
					recordFile.createNewFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
				}
            }
            try {
				final HttpParams httpParameters = new BasicHttpParams();
				// Set the timeout in milliseconds until a connection is established.
				HttpConnectionParams.setConnectionTimeout(httpParameters, httpTimeOut);

				// Set the default socket timeout (SO_TIMEOUT)
				// in milliseconds which is the timeout for waiting for data.
				HttpConnectionParams.setSoTimeout(httpParameters, httpTimeOut);

				final HttpClient client = new DefaultHttpClient(httpParameters);
				
				final HttpGet request = new HttpGet(mainUrl + "create_patient/" + 
						URLEncoder.encode(patientName, "UTF-8") +"/"+ 
						URLEncoder.encode(patientGender, "UTF-8") +"/"+ 
						URLEncoder.encode(patientNumber, "UTF-8") +"/"+ 
						URLEncoder.encode(patientEmergency, "UTF-8") +"/NULL");
				
				final HttpResponse response = client.execute(request);

				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					patientId = EntityUtils.toString(response.getEntity());
				} else {
//					isSaveInServer = false;
				}
				
				final HttpClient client_doctor = new DefaultHttpClient(httpParameters);
				
				final HttpGet request_doctor = new HttpGet(mainUrl + "get_doctor_id/" + 
						URLEncoder.encode(doctorEmail, "UTF-8"));
				
				final HttpResponse response_doctor = client_doctor.execute(request_doctor);
				
				if (response_doctor.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					doctorId = EntityUtils.toString(response_doctor.getEntity());
				} else {
//					isSaveInServer = false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				
//				isSaveInServer = false;
			}
            // create in the server
            if (isSend) {
	            try {
					final HttpParams httpParameters = new BasicHttpParams();
					// Set the timeout in milliseconds until a connection is established.
					HttpConnectionParams.setConnectionTimeout(httpParameters, httpTimeOut);
	
					// Set the default socket timeout (SO_TIMEOUT)
					// in milliseconds which is the timeout for waiting for data.
					HttpConnectionParams.setSoTimeout(httpParameters, httpTimeOut);
	
					final HttpClient client = new DefaultHttpClient(httpParameters);
					
					double longitude = 0;
					double latitude = 0;
					if (location != null) {
						longitude = location.getLongitude();
						latitude = location.getLatitude();
					}
					
					Log.i(TAG, mainUrl + "create_chart_file/" + 
							URLEncoder.encode(recordFileName, "UTF-8").replace("+", "%20") +"/"+ 
							URLEncoder.encode(Double.toString(longitude)+","+Double.toString(latitude), "UTF-8") +"/"+ 
							doctorId +"/"+ 
							patientId +"/0");
					
					final HttpGet request = new HttpGet(mainUrl + "create_chart_file/" + 
							URLEncoder.encode(recordFileName, "UTF-8").replace("+", "%20") +"/"+ 
							URLEncoder.encode(Double.toString(longitude)+","+Double.toString(latitude), "UTF-8") +"/"+ 
							doctorId +"/"+ 
							patientId +"/0");
					
					final HttpResponse response = client.execute(request);
	
					if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
						String responseContent = EntityUtils.toString(response.getEntity());
						
						if (responseContent.equals("successed")) {
							
						} else {
							isCreateFileInServerOK = false;
						}
						Log.d(TAG, "response content: " + responseContent);
					} else {
						isCreateFileInServerOK = false;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
            } else {
            	isCreateFileInServerOK = false;
            }
            
            FileOutputStream outputStream = null;
            try {
				outputStream =  new FileOutputStream(recordFile, true);
				String data = "ECG,RES,ECG_TIME,RES_TIME,R_VALUE,RES_VALUE\n";
				outputStream.write(data.getBytes());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
            if (outputStream != null) {
	            while (running) {
//	            	while (listECGData.size() <= count || listResData.size() <= count 
//	            			|| listECGTime.size() <= count || listResTime.size() <= count
//	            			|| listRSave.size() <= count || listResSave.size() <= count) {
//		            	try {
//							Thread.sleep(5);
//						} catch (InterruptedException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//		            	Log.i(TAG, "local save sleep"+listECGData.size()+":"+count);
//	            	}
	            	try {
						Thread.sleep(5);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            	// save in localPath
	            	String data = Double.toString(this.listECGData.get(count)) +","+ Double.toString(this.listResData.get(count))
	            			+","+ Double.toString(this.listECGTime.get(count)) +","+ Double.toString(this.listResTime.get(count)) 
	            			+","+ Double.toString(this.listRSave.get(count)) +","+ Double.toString(this.listResSave.get(count)) +"\n";
	            	
	            	Log.d(TAG, "save data:"+count);
	            	
	            	try {
						outputStream.write(data.getBytes());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            	
	            	// save in server
//	            	if (isCreateFileInServerOK && isSend) {
//	            		Runnable runnable = new Runnable() {
//							@Override
//							public void run() {
//								// TODO Auto-generated method stub
//								final int finalCount = count;
//								while (listECGData.size() <= finalCount || listResData.size() <= finalCount 
//				            			|| listECGTime.size() <= finalCount || listResTime.size() <= finalCount
//				            			|| listRSave.size() <= finalCount || listResSave.size() <= finalCount) {
//					            	try {
//										Thread.sleep(5);
//									} catch (InterruptedException e) {
//										// TODO Auto-generated catch block
//										e.printStackTrace();
//									}
//					            	Log.i(TAG, "server save sleep"+listECGData.size()+":"+finalCount);
//				            	}
//								try {
//				    				final HttpParams httpParameters = new BasicHttpParams();
//				    				// Set the timeout in milliseconds until a connection is established.
//				    				HttpConnectionParams.setConnectionTimeout(httpParameters, 4000);
//			
//				    				// Set the default socket timeout (SO_TIMEOUT)
//				    				// in milliseconds which is the timeout for waiting for data.
//				    				HttpConnectionParams.setSoTimeout(httpParameters, 4000);
//			
//				    				final HttpClient client = new DefaultHttpClient(httpParameters);
//				    				
//				    				final HttpGet request = new HttpGet(mainUrl + "update_chart_file/" + 
//				    						URLEncoder.encode(recordFileName, "UTF-8").replace("+", "%20") +"/"+ 
//				    						URLEncoder.encode(Double.toString(listECGData.get(finalCount)), "UTF-8") +"/"+ 
//				    						URLEncoder.encode(Double.toString(listResData.get(finalCount)), "UTF-8")+"/"+
//				    						URLEncoder.encode(Long.toString(listECGTime.get(finalCount)), "UTF-8")+"/"+
//				    						URLEncoder.encode(Long.toString(listResTime.get(finalCount)), "UTF-8")+"/"+
//				    						URLEncoder.encode(Double.toString(listRSave.get(finalCount)), "UTF-8")+"/"+
//				    						URLEncoder.encode(Double.toString(listResSave.get(finalCount)), "UTF-8")+"/"+
//				    						finalCount);
//				    				
//				    				final HttpResponse response = client.execute(request);
//			
//				    				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
//				    					String responseContent = EntityUtils.toString(response.getEntity());
//				    					if (responseContent.equals("successed")) {
//				    						
//				    					} else {
//				    						isUpdateDataInServerOK = false;
//				    					}
//				    				} else {
//				    					isUpdateDataInServerOK = false;
//				    				}
//				    			} catch (Exception e) {
//				    				e.printStackTrace();
//				    			}
//							}
//						};
//						
//						executorService.submit(runnable);
//	            	}
	            	
	            	count++;
	            	
//	            	if (count == 30) {
//						Message msg = new Message();
//						msg.what = showSendMsg;
//						mHandler.sendMessage(msg);
//					}
	            	
	            	if (running == false || count >= listECGData.size() || count == 3748) {
						running = false;
						// set the status of chart file
//						Runnable runnable = new Runnable() {
//							
//							@Override
//							public void run() {
//								// TODO Auto-generated method stub
//								try {
//				    				final HttpParams httpParameters = new BasicHttpParams();
//				    				// Set the timeout in milliseconds until a connection is established.
//				    				HttpConnectionParams.setConnectionTimeout(httpParameters, 4000);
//			
//				    				// Set the default socket timeout (SO_TIMEOUT)
//				    				// in milliseconds which is the timeout for waiting for data.
//				    				HttpConnectionParams.setSoTimeout(httpParameters, 4000);
//			
//				    				final HttpClient client = new DefaultHttpClient(httpParameters);
//				    				
//				    				final HttpGet request = new HttpGet(mainUrl + "change_chart_file_status/" + 
//				    						URLEncoder.encode(recordFileName, "UTF-8").replace("+", "%20") +"/"+ 
//				    						"-1");
//				    				
//				    				final HttpResponse response = client.execute(request);
//			
//				    				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
//				    					String responseContent = EntityUtils.toString(response.getEntity());
//				    				} else {
//				    				}
//				    			} catch (Exception e) {
//				    				e.printStackTrace();
//				    			}
//							}
//						};
//						
//						executorService.submit(runnable);
						
						try {
							outputStream.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
	            }
            }
		}
	}
	
	public class GetIdTask extends AsyncTask<Void, Integer, String> {

		@Override
		protected String doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			try {
				final HttpParams httpParameters = new BasicHttpParams();
				// Set the timeout in milliseconds until a connection is established.
				HttpConnectionParams.setConnectionTimeout(httpParameters, httpTimeOut);

				// Set the default socket timeout (SO_TIMEOUT)
				// in milliseconds which is the timeout for waiting for data.
				HttpConnectionParams.setSoTimeout(httpParameters, httpTimeOut);

				final HttpClient client = new DefaultHttpClient(httpParameters);
				
				final HttpGet request = new HttpGet(mainUrl + "create_patient/" + 
						URLEncoder.encode(patientName, "UTF-8") +"/"+ 
						URLEncoder.encode(patientGender, "UTF-8") +"/"+ 
						URLEncoder.encode(patientNumber, "UTF-8") +"/"+ 
						URLEncoder.encode(patientEmergency, "UTF-8") +"/NULL");
				
				
				
				final HttpResponse response = client.execute(request);

				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					patientId = EntityUtils.toString(response.getEntity());
				} else {
					patientId = null;
				}
				
				final HttpClient client_doctor = new DefaultHttpClient(httpParameters);
				
				final HttpGet request_doctor = new HttpGet(mainUrl + "get_doctor_id/" + 
						URLEncoder.encode(doctorEmail, "UTF-8"));
				
				final HttpResponse response_doctor = client_doctor.execute(request_doctor);
				
				if (response_doctor.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					doctorId = EntityUtils.toString(response_doctor.getEntity());
				} else {
					doctorId = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
				
				patientId = null;
				doctorId = null;
			}
			
			Log.w(TAG, "doctor id: "+doctorId+" ; patient id: "+patientId);
			return null;
		}
		
	}
	
	private BroadcastReceiver searchDevicesReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String action = intent.getAction();
			Bundle extras = intent.getExtras();
			Object[] listName = extras.keySet().toArray();

			// device info and detail
			for (int i = 0; i < listName.length; i++) {
				Log.i(TAG, String.valueOf(extras.get(listName[i].toString())));
			}

			BluetoothDevice btDevice = null;
			// seach device and get the mac address
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				btDevice = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

				String strDeviceInfo = btDevice.getName() + "|"
						+ btDevice.getAddress();

				if (strDeviceInfo != null) {
					if (listBTDevices.indexOf(strDeviceInfo) == -1) {
						listBTDevices.add(strDeviceInfo);
					}
				}
				
//				adapter.notifyDataSetChanged();
			} else if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
				btDevice = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				switch (btDevice.getBondState()) {
				case BluetoothDevice.BOND_BONDING:
					Log.d(TAG, "配對中...");
					break;
				case BluetoothDevice.BOND_BONDED:
					Log.d(TAG, "完成配對");
					break;
				case BluetoothDevice.BOND_NONE:
					Log.d(TAG, "取消配對");
					break;
				}
			} else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
				btDevice = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				
				isConnected = false;
				
				Message msg = new Message();
				msg.what = showToast;
				
				Bundle data = new Bundle();
				data.putString("MESSAGE", "裝置 "+ btDevice.getName() +" 已中斷連接");
				
				msg.setData(data);
				mHandler.sendMessage(msg);
				
				if (receiveBThread != null)
					receiveBThread.stopThread();

				if (btIn != null) {
					try {
						btSocket.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	};
	
	public class ReadDataTask extends AsyncTask<Void, Integer, String> {
		boolean isSaveInServer = true;
		boolean isReadData = true;

		@Override
		protected String doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			// save patient's data to database
//			try {
//				final HttpParams httpParameters = new BasicHttpParams();
//				// Set the timeout in milliseconds until a connection is established.
//				HttpConnectionParams.setConnectionTimeout(httpParameters, httpTimeOut);
//
//				// Set the default socket timeout (SO_TIMEOUT)
//				// in milliseconds which is the timeout for waiting for data.
//				HttpConnectionParams.setSoTimeout(httpParameters, httpTimeOut);
//
//				final HttpClient client = new DefaultHttpClient(httpParameters);
//				
//				final HttpGet request = new HttpGet(mainUrl + "create_patient/" + 
//						URLEncoder.encode(patientName, "UTF-8") +"/"+ 
//						URLEncoder.encode(patientGender, "UTF-8") +"/"+ 
//						URLEncoder.encode(patientNumber, "UTF-8") +"/"+ 
//						URLEncoder.encode(patientEmergency, "UTF-8") +"/NULL");
//				
//				final HttpResponse response = client.execute(request);
//
//				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
//					patientId = EntityUtils.toString(response.getEntity());
//				} else {
////					isSaveInServer = false;
//				}
//				
//				final HttpClient client_doctor = new DefaultHttpClient(httpParameters);
//				
//				final HttpGet request_doctor = new HttpGet(mainUrl + "get_doctor_id/" + 
//						URLEncoder.encode(doctorEmail, "UTF-8"));
//				
//				final HttpResponse response_doctor = client_doctor.execute(request_doctor);
//				
//				if (response_doctor.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
//					doctorId = EntityUtils.toString(response_doctor.getEntity());
//				} else {
////					isSaveInServer = false;
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//				
////				isSaveInServer = false;
//			}
			
			// read the data
			if (isSaveInServer) {
				
				File recordFile = new File(Environment.getExternalStorageDirectory().getPath() +File.separator+ "Data2.csv");
	            if (!recordFile.exists()) {
	            	try {
						recordFile.createNewFile();
					} catch (IOException e) {
						// TODO Auto-generated catch block
//						e.printStackTrace();
					}
	            }
	            
	            FileOutputStream outputStream = null;
	            try {
					outputStream =  new FileOutputStream(recordFile, true);
					String data = "ECG,ECG_FILTER,RES,RES_FILTER,ECG_RATE,RES_RATE\n";
					outputStream.write(data.getBytes());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				String dataPath = Environment.getExternalStorageDirectory().getPath() +File.separator+ "Data.csv";
				Log.d(TAG, "Data path:" + dataPath);
				
				// read the data
				int firstRPosition = 0;
				double firstRValue = Double.MIN_VALUE;
				
				listECGData = new ArrayList<Double>();
				listECGDataFilter = new ArrayList<Double>();
				listResData = new ArrayList<Double>();
				listResDataFilter = new ArrayList<Double>();
				listECGTime = new ArrayList<Long>();
				listResTime = new ArrayList<Long>();
				
				listRPosition = new ArrayList<Integer>();
				listRValue = new ArrayList<Double>();
				listRSave = new ArrayList<Double>();
				listResPosition = new ArrayList<Integer>();
				listResSave = new ArrayList<Double>();
				
				FileInputStream is = null; 
				BufferedReader reader = null;
				int count = 0;
			    try {
			    	is = new FileInputStream(dataPath); 	
					reader = new BufferedReader(new InputStreamReader(is));
			        String line;
			        while ((line = reader.readLine()) != null) {
			        	String[] rowData = line.split(",");
			        	String ecg = rowData[3];
			        	String res = rowData[0];
			        	
			        	if (count == 0) {
			        		count++;
			        		continue;
			        	}
			        	
			        	listECGData.add(Double.valueOf(ecg));
			        	listResData.add(Double.valueOf(res));
			        	listECGDataFilter.add(Double.valueOf(ecg));
			        	listResDataFilter.add(Double.valueOf(res));
			        	
						listRSave.add(0.00);
						listResSave.add(0.00);
			        	
			        	double ecgData = 0;
			        	double resData = 0;
			        	if (listECGData.size() > FilterCoefficient.ECG_COEFF.length) {
			        		for (int i = 0; i < FilterCoefficient.ECG_COEFF.length; i++) {
			        			ecgData += listECGData.get(listECGData.size()-FilterCoefficient.ECG_COEFF.length+i)*FilterCoefficient.ECG_COEFF[i];
			        		}
			        		
			        		listECGDataFilter.set(listECGDataFilter.size()-1, ecgData);	
			        	}
			        	
			        	if (listResData.size() > FilterCoefficient.RES_COEFF.length) {
			        		for (int i = 0; i < FilterCoefficient.RES_COEFF.length; i++) {
			        			resData += listResData.get(listResData.size()-FilterCoefficient.RES_COEFF.length+i)*FilterCoefficient.RES_COEFF[i];
			        		}
			        		
			        		listResDataFilter.set(listResDataFilter.size()-1, resData);
			        	}
			        	
			        	/* calculate the ecg rate */
						if (listECGData.size() >= 502) {
							double slope = -2*listECGDataFilter.get(listECGDataFilter.size()-5)-listECGDataFilter.get(listECGDataFilter.size()-4)
									+listECGDataFilter.get(listECGDataFilter.size()-2)+2*listECGDataFilter.get(listECGDataFilter.size()-1);
							if (listECGDataFilter.size() <= 502+120) {
								if (slope > maxi) {
									maxi = slope;
									slopeTh = 0.5*maxi;
								}
							} else {
								double slope_1 = -2*listECGDataFilter.get(listECGDataFilter.size()-6)-listECGDataFilter.get(listECGDataFilter.size()-5)
										+listECGDataFilter.get(listECGDataFilter.size()-3)+2*listECGDataFilter.get(listECGDataFilter.size()-2);
								
								if (listRPosition.isEmpty()) {
									// first R
									if (slope > slopeTh && slope_1 > slopeTh && firstRPosition == 0) {
										rStartPosition = listECGData.size()-3;
										rStartValue = listECGDataFilter.get(listECGDataFilter.size()-3);
										
										firstRPosition = listECGData.size()-3;
										firstRValue = listECGDataFilter.get(listECGDataFilter.size()-3);
									}
									
									if (firstRPosition != 0) {
										if (listECGData.size()-3 < rStartPosition+20) {
											if (listECGDataFilter.get(listECGDataFilter.size()-3) > firstRValue) {
												firstRPosition = listECGDataFilter.size()-3;
												firstRValue = listECGDataFilter.get(listECGDataFilter.size()-3);
											}
										} else {
											listRPosition.add(firstRPosition);
											listRValue.add(firstRValue);
											
											maxi = ((listRValue.get(0)-rStartValue)-maxi)/16+maxi;
											slopeTh = 0.5*maxi;
											
											firstRPosition = 0;
											firstRValue = Double.MIN_VALUE;
										}
									}
								} else {
									// update maxi
									if (listECGData.size()-3 > listRPosition.get(listRPosition.size()-1)+60) {
										if (slope > slopeTh && slope_1 > slopeTh && firstRPosition == 0) {
											rStartPosition = listECGData.size()-3;
											rStartValue = listECGDataFilter.get(listECGDataFilter.size()-3);
											
											firstRPosition = listECGData.size()-3;
											firstRValue = listECGDataFilter.get(listECGDataFilter.size()-3);
										}
										
										if (firstRPosition != 0) {
											if (listECGData.size()-3 < rStartPosition+20) {
												if (listECGDataFilter.get(listECGDataFilter.size()-3) > firstRValue) {
													firstRPosition = listECGData.size()-3;
													firstRValue = listECGDataFilter.get(listECGDataFilter.size()-3);
												}
											} else {
												listRPosition.add(firstRPosition);
												listRValue.add(firstRValue);
												
												maxi = ((listRValue.get(listRValue.size()-1)-rStartValue)-maxi)/16+maxi;
												slopeTh = 0.5*maxi;
												
												firstRPosition = 0;
												firstRValue = Double.MIN_VALUE;
											}
										}
									}
								}
							}
							
							if (listRPosition.size() > 1) {
								
								double diffTime = 0.008*(double)(listRPosition.get(listRPosition.size()-1)-listRPosition.get(listRPosition.size()-2));
								
//								Log.d(TAG, "ecg rate:"+diffTime+";"+listRPosition.get(listRPosition.size()-1)+";"
//								+listRPosition.get(listRPosition.size()-2)+";"+listECGTime.get(listRPosition.get(listRPosition.size()-1))+";"+
//								listECGTime.get(listRPosition.get(listRPosition.size()-2)));
								
								if (diffTime != 0) {
									double rate = 60/diffTime;
									if (rate > 150 || rate < 40) {
										if (rate < 10 && rate >= 0) {
											
										} else {
											rate = 99;
										}
									}
									
									listRSave.set(listECGData.size()-1, rate);
								}
							}
						}
						
						/* calculate the res rate */
						if (listResData.size() >= 201) {
							if ((listResDataFilter.get(listResDataFilter.size()-1) > listResDataFilter.get(listResDataFilter.size()-2)) && resStartValue == 0) {
								resStartValue = listResDataFilter.size()-1;
							}
							
							if (resStartValue != 0) {
								if (listResDataFilter.get(listResDataFilter.size()-2) >= listResDataFilter.get(listResDataFilter.size()-1)) {
									resEndValue = listResDataFilter.size()-2;
								}
							}
							
							if (resStartValue != 0 && resEndValue != 0) {
								if (Math.abs(listResDataFilter.get(resStartValue)-listResDataFilter.get(resEndValue)) > 1000) {
									listResPosition.add(resEndValue);
									
									resStartValue = 0;
									resEndValue = 0;
								} else {
									resEndValue = 0;
								}
							}
							
							if (listResPosition.size() > 1) {
								double diffTime = 0.008*(double)(listResPosition.get(listResPosition.size()-1)-listResPosition.get(listResPosition.size()-2));
								
//								Log.d(TAG, "res rate:"+diffTime+";"+listResPosition.get(listResPosition.size()-1)+";"
//										+listResPosition.get(listResPosition.size()-2)+";"+listResTime.get(listResPosition.get(listResPosition.size()-1))+";"+
//										listResTime.get(listResPosition.get(listResPosition.size()-2)));
								
								if (diffTime != 0) {
									
									listResSave.set(listResData.size()-1, 60/diffTime);
								}
							}
						}
			        	
			        	String data = Double.toString(listECGData.get(listECGData.size()-1)) +","+ Double.toString(listECGDataFilter.get(listECGDataFilter.size()-1)) +","+
		        				Double.toString(listResData.get(listResData.size()-1))+","+Double.toString(listResDataFilter.get(listResDataFilter.size()-1))+","+
		        				Double.toString(listRSave.get(listRSave.size()-1))+","+Double.toString(listResSave.get(listResSave.size()-1))+"\n";
	            	
		            	try {
							outputStream.write(data.getBytes());
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
							
//			        	publishProgress(i, sheet.getRows());
							
//						Log.d(TAG, "ECG: " + ecg + " ; Res: " + res + " .");
			        }
			    } catch (Exception e) {
			    	// TODO Auto-generated catch block
					e.printStackTrace();
					
					isReadData = false;
			    } finally {
			    	try {
			    		if (reader != null)
			    			reader.close();
				    	if (is != null)
				    		is.close();
			    	} catch (Exception e) {
			            e.printStackTrace();
			        }
			    	
			    	try {
						outputStream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    }
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			
//			getSherlock().setProgressBarIndeterminateVisibility(true);
			progressDialog = new ProgressDialog(MainActivity.this);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setIndeterminate(true);
//			progressDialog.setMax(100);
			progressDialog.setMessage("Reading data...");
			progressDialog.setProgressNumberFormat("");
	        progressDialog.setCancelable(false);
	        progressDialog.show();
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
//			int progress = (int)((float)values[0] / (float)values[1] * (Window.PROGRESS_END - Window.PROGRESS_START));
//			setSupportProgress(progress);
			
			int progress = (int)((float)values[0] / (float)values[1] * 100);
			progressDialog.setProgress(progress);
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
			if (progressDialog != null && progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
//			setSupportProgress(Window.PROGRESS_END - Window.PROGRESS_START);
//			getSherlock().setProgressBarIndeterminateVisibility(false);
			
			if (progressDialog != null && progressDialog.isShowing()) {
				progressDialog.setProgress(100);
				progressDialog.dismiss();
			}
			
			if (!isSaveInServer) {
				Toast.makeText(
						MainActivity.this,
						"請檢查您的網路狀態",
						Toast.LENGTH_LONG).show();
			} else if (!isReadData) {
				Toast.makeText(
						MainActivity.this,
						"目前沒有任何儲存裝置",
						Toast.LENGTH_LONG).show();
			} else {
//				if (showDataThread == null || !showDataThread.isAlive()) {
//					showDataThread = new ShowDataThread();
//					showDataThread.start();
//				}
				
//				if (localPath != null && (saveDataThread == null || !saveDataThread.isAlive())) {
//					saveDataThread = new SaveDataThread();
//					saveDataThread.start();
//				}
			}
		}
	}
}
