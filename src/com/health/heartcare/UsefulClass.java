package com.health.heartcare;

import java.io.File;

import android.os.Environment;
import android.os.StatFs;

public class UsefulClass {
	/* SD 卡檔案處理 */
	// SD 卡是否存在及可讀寫性
	public boolean hasStorage(boolean requireWriteAccess) {
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		} else if (!requireWriteAccess
				&& Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			return true;
		}
		return false;
	}

	// 取得 SD 有效空間
	public long getFreeSpace() {
		long freeSpace = 0;

		StatFs stat = new StatFs(Environment.getExternalStorageDirectory()
				.getPath());
		freeSpace = (long) stat.getBlockSize()
				* (long) stat.getAvailableBlocks();
		// Log.d(TAG, "free memory: "+freeSpace);
		return freeSpace;
	}

	// 資料夾是否存在
	public boolean dirExist(String dirPath) {
		File dirPathFile = new File(dirPath);
		if (dirPathFile.isDirectory())
			return true;
		else
			return false;
	}

	// 建資料夾
	public boolean createFolder(String path, String folderName) {
		if (hasStorage(true)) {
			File directory = new File(path + "/" + folderName);
			if (!directory.isDirectory()) {
				if (directory.mkdir())
					return true;
				else
					return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	// 刪檔
	public boolean deleteFile(String path, String folderName) {
		if (hasStorage(true)) {
			File wantDelete = new File(path + "/" + folderName);
			if (wantDelete.exists()) {
				if (wantDelete.delete()) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
