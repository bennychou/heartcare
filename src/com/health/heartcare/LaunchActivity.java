package com.health.heartcare;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class LaunchActivity extends SherlockActivity {
	Thread thread;
	ImageView imgLauncher;
	boolean isBack = false;

	final String TAG = "LaunchActivity";
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		// 設定全螢幕(要在setContentView之前)
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_launch);
		
		BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
		if (btAdapter == null) {
			Toast.makeText(this, "請確認您手機的藍芽裝置",
					Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		
		btAdapter.enable();

		thread = new LauncherThread();
		thread.start();
	}
	
	private final int gotoMain = 0;
	private final int finish = 1;
	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case gotoMain:
				Intent main = new Intent();
				main.setClass(LaunchActivity.this, MainActivity.class);
				startActivity(main);
				finish();
				break;
			case finish:
				finish();
				break;
			}
			super.handleMessage(msg);
		}
	};

	class LauncherThread extends Thread {
		@Override
		public void run() {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (isBack) {
				Message msg = new Message();
				msg.what = finish;
				mHandler.sendMessage(msg);
			} else {
				Message msg = new Message();
				msg.what = gotoMain;
				mHandler.sendMessage(msg);
			}
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		isBack = true;
	}

//	@Override
//	public void onAttachedToWindow() {
//		// TODO Auto-generated method stub
//		super.onAttachedToWindow();
//		this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
//	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_HOME)
			isBack = true;
		return super.onKeyDown(keyCode, event);
	}

}
