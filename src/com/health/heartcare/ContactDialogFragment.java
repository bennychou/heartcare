package com.health.heartcare;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.AlertDialog.Builder;

import com.actionbarsherlock.app.SherlockDialogFragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.support.v4.app.DialogFragment;
import android.view.TextureView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ContactDialogFragment extends SherlockDialogFragment {
	SharedPreferences settingPrefs;
	
	public ContactDialogFragment() {
//        setDialogType(DialogType.AlertDialog);
    }
	
	public static ContactDialogFragment newInstance(String title, String content, String tag) {
		ContactDialogFragment cdf = new ContactDialogFragment();
		Bundle args = new Bundle();
		args.putString("title", title);
		args.putString("content", content);
		args.putString("tag", tag);
		cdf.setArguments(args);
		return cdf;
	}
	@Override
    public AlertDialog onCreateDialog(Bundle savedInstanceState) {
		settingPrefs = getSherlockActivity().getSharedPreferences(SettingsActivity.Settings, Context.MODE_PRIVATE);
		
        AlertDialog.Builder builder = new AlertDialog.Builder(getSherlockActivity());
        builder.setTitle(getArguments().getString("title"));
        builder.setIcon(R.drawable.ic_launcher);
        View view = View.inflate(getSherlockActivity(),
				R.layout.dialog_contact, null);
        final TextView textSubject = (TextView) view.findViewById(R.id.text_subject);
        final EditText editContent = (EditText) view.findViewById(R.id.edit_content);
        editContent.setText(getArguments().getString("content"));
        builder.setView(view);
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.setPositiveButton(android.R.string.ok, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				SharedPreferences.Editor editor = settingPrefs.edit();
				editor.putString(getArguments().getString("tag"), editContent.getText().toString());
				editor.commit();
				
				((SettingsActivity) getSherlockActivity()).refreshAdapter();
			}
		});
        return builder.create();
    }
}
