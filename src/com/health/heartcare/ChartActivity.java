package com.health.heartcare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;

import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.Toast;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.fima.chartview.ChartView;
import com.fima.chartview.LinearSeries;
import com.fima.chartview.LinearSeries.LinearPoint;
import com.health.heartcare.ValueLabelAdapter.LabelOrientation;

public class ChartActivity extends SherlockActivity {
	
	ArrayList<Double> listECGData;
	ArrayList<Double> listResData;
	
	ArrayList<Long> listECGTime;
	ArrayList<Long> listResTime;
	
	ArrayList<Double> listRSave;
	ArrayList<Double> listResSave;
	
	LinearSeries seriesECG;
	LinearSeries seriesRes;

	ChartView chartHeart;
	ChartView chartBreath;
	
	TextView textName;
	TextView textECG;
	TextView textRes;
	
	SeekBar seekBar;
	
	ProgressDialog progressDialog;
	
	File chartFile;
	
	ReadDataTask readDataTask;
	
	final String TAG = "ChartActivity"; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setTheme(R.style.Holo_Theme_Light_DarkActionBar); // set the theme
		/* set screen always bright */
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		requestWindowFeature(Window.FEATURE_PROGRESS);

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
			requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.activity_chart);
		
		getSherlock().setProgressBarIndeterminateVisibility(false);
		
		/* get extras */
		chartFile = (File) getIntent().getExtras().getSerializable("chartFile");
		
		/* set action bar */
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowHomeEnabled(true);
		actionBar.setLogo(getResources().getDrawable(R.drawable.ic_main_logo));
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.ic_main_actionbar));
		
		findViews();
		
		textName.setText(chartFile.getName());
		
		/* read the data */
		readDataTask = new ReadDataTask();
		readDataTask.execute();
	}
	
	private void findViews() {
		chartHeart = (ChartView) findViewById(R.id.chart_heart);
		chartBreath = (ChartView) findViewById(R.id.chart_breath);
		
		textName = (TextView) findViewById(R.id.text_name);
		textECG = (TextView) findViewById(R.id.text_ecg);
		textRes = (TextView) findViewById(R.id.text_res);
		
		seekBar = (SeekBar) findViewById(R.id.seekBar);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	OnSeekBarChangeListener seekBarChangeListener = new OnSeekBarChangeListener() {
		
		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			// TODO Auto-generated method stub
			/* set the line attributes */
			seriesECG = new LinearSeries();
			seriesECG.setLineColor(0xFFFF9100);
			seriesECG.setLineWidth(5);
			
			seriesRes = new LinearSeries();
			seriesRes.setLineColor(0xFFFF9100);
			seriesRes.setLineWidth(5);
			
			for (int i = progress; i < progress+MainActivity.SHOW_NUMBER; i++) {
				/* add 10 data */
				seriesECG.addPoint(new LinearPoint(i, listECGData.get(i)));
				seriesRes.addPoint(new LinearPoint(i, listResData.get(i)));
			}
			
			textECG.setText(String.format("%1$.2f", listRSave.get(progress+MainActivity.SHOW_NUMBER-1)));
			textRes.setText(String.format("%1$.2f", listResSave.get(progress+MainActivity.SHOW_NUMBER-1)));
			
			chartHeart.clearSeries();
			chartHeart.addSeries(seriesECG);
			chartHeart.setLeftLabelAdapter(new ValueLabelAdapter(ChartActivity.this, LabelOrientation.VERTICAL));
			chartHeart.setBottomLabelAdapter(new ValueLabelAdapter(ChartActivity.this, LabelOrientation.HORIZONTAL));
			
			chartBreath.clearSeries();
			chartBreath.addSeries(seriesRes);
			chartBreath.setLeftLabelAdapter(new ValueLabelAdapter(ChartActivity.this, LabelOrientation.VERTICAL));
			chartBreath.setBottomLabelAdapter(new ValueLabelAdapter(ChartActivity.this, LabelOrientation.HORIZONTAL));
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
			
		}
	};

	public class ReadDataTask extends AsyncTask<Void, Integer, String> {
		boolean isReadData = true;

		@Override
		protected String doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			
			// read the data
			listECGData = new ArrayList<Double>();
			listResData = new ArrayList<Double>();
			
			listECGTime = new ArrayList<Long>();
			listResTime = new ArrayList<Long>();
			
			listRSave = new ArrayList<Double>();
			listResSave = new ArrayList<Double>();
			
			FileInputStream is = null; 
			BufferedReader reader = null;
			int count = 0;
		    try {
		    	is = new FileInputStream(chartFile); 	
				reader = new BufferedReader(new InputStreamReader(is));
		        String line;
		        while ((line = reader.readLine()) != null) {
		        	String[] rowData = line.split(",");
		        	String ecg = rowData[0];
		        	String res = rowData[1];
		        	String ecgTime = rowData[2];
		        	String resTime = rowData[3];
		        	String rSave = rowData[4];
		        	String resSave = rowData[5];
		        	
		        	if (count == 0) {
		        		count++;
		        		continue;
		        	}
		        	
		        	listECGData.add(Double.valueOf(ecg));
		        	listResData.add(Double.valueOf(res));
		        	
//		        	listECGTime.add(Double.valueOf(ecgTime));
//		        	listResTime.add(Double.valueOf(resTime));
		        	
		        	listRSave.add(Double.valueOf(rSave));
		        	listResSave.add(Double.valueOf(resSave));
						
//		        	publishProgress(i, sheet.getRows());
						
//					Log.d(TAG, "ECG: " + ecg + " ; Res: " + res + " .");
		        }
		    }
		    catch (Exception e) {
		    	// TODO Auto-generated catch block
				e.printStackTrace();
				
				isReadData = false;
		    } finally {
		    	try {
		    		if (reader != null)
		    			reader.close();
			    	if (is != null)
			    		is.close();
		    	} catch (Exception e) {
		            e.printStackTrace();
		        }
		    }
			return null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			
//			getSherlock().setProgressBarIndeterminateVisibility(true);
			progressDialog = new ProgressDialog(ChartActivity.this);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setIndeterminate(true);
//			progressDialog.setMax(100);
			progressDialog.setMessage("Reading data...");
			progressDialog.setProgressNumberFormat("");
	        progressDialog.setCancelable(false);
	        progressDialog.show();
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
//			int progress = (int)((float)values[0] / (float)values[1] * (Window.PROGRESS_END - Window.PROGRESS_START));
//			setSupportProgress(progress);
			
			int progress = (int)((float)values[0] / (float)values[1] * 100);
			progressDialog.setProgress(progress);
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
			if (progressDialog != null && progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
//			setSupportProgress(Window.PROGRESS_END - Window.PROGRESS_START);
//			getSherlock().setProgressBarIndeterminateVisibility(false);
			
			if (progressDialog != null && progressDialog.isShowing()) {
//				progressDialog.setProgress(100);
				progressDialog.dismiss();
			}
			
			if (!isReadData) {
				Toast.makeText(
						ChartActivity.this,
						"�ɮצ����~",
						Toast.LENGTH_LONG).show();
			
				ChartActivity.this.finish();
			} else {
				/* set the line attributes */
				seriesECG = new LinearSeries();
				seriesECG.setLineColor(0xFFFF9100);
				seriesECG.setLineWidth(5);
				
				seriesRes = new LinearSeries();
				seriesRes.setLineColor(0xFFFF9100);
				seriesRes.setLineWidth(5);
				
				for (int i = 0; i < MainActivity.SHOW_NUMBER; i++) {
					/* add 10 data */
					seriesECG.addPoint(new LinearPoint(i, listECGData.get(i)));
					seriesRes.addPoint(new LinearPoint(i, listResData.get(i)));
				}
				
				
				textECG.setText(String.format("%1$.2f", listRSave.get(MainActivity.SHOW_NUMBER-1)));
				textRes.setText(String.format("%1$.2f", listResSave.get(MainActivity.SHOW_NUMBER-1)));
				
				chartHeart.clearSeries();
				chartHeart.addSeries(seriesECG);
				chartHeart.setLeftLabelAdapter(new ValueLabelAdapter(ChartActivity.this, LabelOrientation.VERTICAL));
				chartHeart.setBottomLabelAdapter(new ValueLabelAdapter(ChartActivity.this, LabelOrientation.HORIZONTAL));
				
				chartBreath.clearSeries();
				chartBreath.addSeries(seriesRes);
				chartBreath.setLeftLabelAdapter(new ValueLabelAdapter(ChartActivity.this, LabelOrientation.VERTICAL));
				chartBreath.setBottomLabelAdapter(new ValueLabelAdapter(ChartActivity.this, LabelOrientation.HORIZONTAL));
				
				// set the seekbar
				seekBar.setMax(listECGData.size()-MainActivity.SHOW_NUMBER);
				seekBar.setOnSeekBarChangeListener(seekBarChangeListener);
			}
		}
	}
}
